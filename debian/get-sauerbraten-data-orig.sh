#!/bin/sh

RELEASEDATE=20201227
SVNREV=6491

svn co -r ${SVNREV} \
    https://svn.code.sf.net/p/sauerbraten/code/ \
    sauerbraten-0.0.${RELEASEDATE}

find sauerbraten-0.0.${RELEASEDATE} -type d -name '.svn' \
    -exec rm -rf "{}" \;
rm -rf sauerbraten-0.0.${RELEASEDATE}/vcpp
rm -rf sauerbraten-0.0.${RELEASEDATE}/xcode
rm -rf sauerbraten-0.0.${RELEASEDATE}/lib
rm -rf sauerbraten-0.0.${RELEASEDATE}/bin
rm -rf sauerbraten-0.0.${RELEASEDATE}/bin64
rm -rf sauerbraten-0.0.${RELEASEDATE}/sauerbraten.app
rm -rf sauerbraten-0.0.${RELEASEDATE}/bin_unix
rm -f sauerbraten-0.0.${RELEASEDATE}/sauerbraten.pdb
rm -f sauerbraten-0.0.${RELEASEDATE}/*.bat
rm -f sauerbraten-0.0.${RELEASEDATE}/sauerbraten_unix
rm -f sauerbraten-0.0.${RELEASEDATE}/server-init.cfg
rm -rf sauerbraten-0.0.${RELEASEDATE}/src

tar caf sauerbraten_0.0.${RELEASEDATE}.orig.tar.xz \
    sauerbraten-0.0.${RELEASEDATE}

rm -rf sauerbraten-0.0.${RELEASEDATE}
